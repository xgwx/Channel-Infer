%Location for CBS, TBS, TBS2
L_c = [499.53139326,568.018599,22];
L_t = [627.1252938,529.9704305,3];
L_t2 = [527.110976,530.116047,3];

load('.\Data\WI_MIMO_Data_Para_TBS_HO_28000MHz.mat');
angle_t = angle;
cir_t = cir;
loc_error = [5,1,0];
N_sam = 100;
load('.\Data\HO_loc_data');
[N_MS,~] = size(LocData);
cos_th = zeros(1,N_MS);
cos_th2 = zeros(1,N_MS);
Beam_Norm = 256;
Beam_True = 32;
y_loc = zeros(1,N_MS);
%Loc_perform = zeros(1,N_MS);
loc_err_perform = zeros(3,N_MS);
loc_bps_perform = zeros(3,N_MS);
P = 1;
N = 1e-12;
for i = 1:N_MS
    H1 = BeamResponse_gscm_New(cir_t{i},angle_t{i},Beam_true,Beam_Norm,0.5);
    for i_sp = 1:N_sam
        for i_err = 1:length(loc_error)
			%Calulate the DoD between the transceivers
            temp_vec = LocData(i,:) - L_t + 1/sqrt(2)*loc_error(i_err)*(randn(1)*[1,0,0]+randn(1)*[0,1,0]);
            cos_th(i) = temp_vec(2)/norm(temp_vec);
            %cos_th2(i) = sin(angle_t{i}(1,3))*sin(angle_t{i}(1,4));
			%Reconstruct the CSI based on location information
            H = exp(-1j*2*pi*0.5*cos_th(i)*(0:Beam_True-1).');
            perform = abs(fft(H,Beam_Norm));
            [~,loc_bp] = max(perform);
            loc_err_perform(i_err,i) = loc_err_perform(i_err,i) + H1(loc_bp)/max(H1);
            loc_bps_perform(i_err,i) = loc_bps_perform(i_err,i) + log2(1+P*(H1(loc_bp))^2/Beam_True/N);
        end
    end
end
loc_err_perform = loc_err_perform/N_sam;
loc_bps_perform = loc_bps_perform/N_sam;
save('loc_perform28GHz_beam256_32_New.mat','loc_err_perform','loc_error','loc_bps_perform');
