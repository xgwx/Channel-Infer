%% Load Channel Parameters
N_CBSA = 128;
N_TBSA = 32;
N_UEA = 1;
N_beampattern = 256; % Beam codebook size
% Carrier_Freq = 28000e6;
load('.\Data\WI_MIMO_Data_Para_TBS_28000MHz.mat');
angle_c = angle;
cir_c = cir;
load('.\Data\WI_MIMO_Data_Para_TBS_28000MHz.mat');
angle_t = angle;
cir_t = cir;
load('.\Data\HO_loc_data');
[N_MS,~] = size(LocData);
X_min = min(LocData(:,1));
X_max = max(LocData(:,1));
y_min = min(LocData(:,2));
y_max = max(LocData(:,2));
%% Channel Sequences Generating
N_path = 1000; %Number of HO paths
N_samplepoint = 201;
N_samplepath = 601;
N_point_max = 1500;
ts = 1e-3;
X = cell(1,N_path);
y = cell(1,N_path);
loc = cell(1,N_path);
X1 = []; y1=[];
for i = 1:N_path
    %generate path index
    y_loc = randi(N_samplepath); %Vehicles move on a line
    v_init = 10+5*rand();  %Random speed
    a_init = 6*rand()-3;   %Ramdom acceleration
    loc_tmp = zeros(N_point_max,1);
    for j = 1:N_point_max
        tmp_dis = X_min+v_init*(j-1)*ts+0.5*a_init*((j-1)*ts)^2;
        loc_tmp(j,1) = round((tmp_dis-X_min)/0.05)+1; %Using the closet point on the grid to approximate
        if(loc_tmp(j,1)>N_samplepoint)
            loc_tmp = loc_tmp(1:j-1,1); %Terminate
            break;
        end
    end
    tmp_pathlen = length(loc_tmp);
    loc{i} = [loc_tmp,y_loc*ones(tmp_pathlen,1)]; %Generate the locations of the trace
    %generate processed csi of CBS
    X_tmp = zeros(tmp_pathlen,N_CBSA*N_UEA);
    i_path_tmp = N_samplepoint*(y_loc-1)*ones(tmp_pathlen,1)+loc_tmp; %The corresponding index in the generated channel
    for jj = 1:tmp_pathlen
        i_user_tmp = i_path_tmp(jj);
        channel_tmp = fft2(CSI_gscm(cir_c{1,i_user_tmp},angle_c{i_user_tmp},N_CBSA,N_UEA,0.5));
        X_tmp(jj,:) = reshape(log(abs(channel_tmp)),[1,N_CBSA*N_UEA]);
    end
    X{1,i} = X_tmp;
    X1 = [X1;X_tmp];
    %generate beam pattern of TBS
    y_tmp = zeros(tmp_pathlen,1);
    for jj = 1:tmp_pathlen
        i_user_tmp = i_path_tmp(jj);
        y_tmp(jj,:) = BeamPattern_gscm(cir_t{1,i_user_tmp},angle_t{i_user_tmp},N_beampattern,N_beampattern,0.5);
    end
    y{1,i} = y_tmp;
end
for i = 1:N_path
    X{i} = X{i} - mean(X1(:));
    X{i} = X{i}/std(X1(:));
end
%save('WI_MIMO_Path_2600MHz_Dense_sp3.mat','X','y');


N_train = round(N_path*0.9); % 90 percents for the training set
N_test = N_path - N_train;
X_train = cell(1,N_train);
y_train = cell(1,N_train);
X_test = cell(1,N_test);
y_test = cell(1,N_test);
for i = 1:N_train
    X_train{i} = X{i};
    y_train{i} = y{i};
end

for i = 1:N_test
    X_test{i} = X{i+N_train};
    y_test{i} = y{i+N_train};
end
save('./Data/WI_MIMO_real_tt_28000MHz_Beam256_sp7.mat','X_train','y_train','X_test','y_test','loc');