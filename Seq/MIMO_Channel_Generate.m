%% Loading Parameters From Generated Data
% HO stands for Handover case
StdPath = '..\Data\HO';
%rxSet006: Handover Rx sets
rxSet = 'rxSet006';
%txSet001: Macro BS; txSet002: Target RSU; txSet005:source RSU
txSet = 'txSet001';
%% Load Channel Parameters
CirFile = dir([StdPath,'cir\*',txSet,'*',rxSet,'*.csv']);
N_UE1 = length(CirFile);
AoAFile = dir([StdPath,'doa\*',txSet,'*',rxSet,'*.csv']);
N_UE2 = length(AoAFile);
if(N_UE1~=N_UE2)
    print('users incorrect');
end
cir = cell(1,N_UE1);
angle = cell(1,N_UE1);
for i = 1:N_UE1
    if(i>99)
        temp_cir_file = [StdPath,'cir\cir.',txSet,'.txPt001.',rxSet,'.rxPt',num2str(i),'.txEl001.rxEl001.inst001.csv'];
        temp_doa_file = [StdPath,'doa\angles.',txSet,'.txPt001.',rxSet,'.rxPt',num2str(i),'.txEl001.rxEl001.csv'];
    elseif(i>9)
        temp_cir_file = [StdPath,'cir\cir.',txSet,'.txPt001.',rxSet,'.rxPt0',num2str(i),'.txEl001.rxEl001.inst001.csv'];
        temp_doa_file = [StdPath,'doa\angles.',txSet,'.txPt001.',rxSet,'.rxPt0',num2str(i),'.txEl001.rxEl001.csv'];
    else
        temp_cir_file = [StdPath,'cir\cir.',txSet,'.txPt001.',rxSet,'.rxPt00',num2str(i),'.txEl001.rxEl001.inst001.csv'];
        temp_doa_file = [StdPath,'doa\angles.',txSet,'.txPt001.',rxSet,'.rxPt00',num2str(i),'.txEl001.rxEl001.csv'];
    end
temp = csvread(temp_cir_file,1,0);
cir{1,i} = temp(:,3:5);
temp1 = csvread(temp_doa_file,1,0);
[npath,~] = size(temp1);
angle{1,i} = temp1(:,3:7);
end
save('.\Data\WI_MIMO_Data_Para_MBS_28000MHz.mat','cir','angle');