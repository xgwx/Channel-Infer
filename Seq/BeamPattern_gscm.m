function [N] = BeamPattern_gscm(cir,angle,N_BP,N_beam,d)
%BeamPattern_gscm Generating the optimal beam direction based on GSCM, UEs with single antennas
H = zeros(N_BP,1);
[N_mpath,~] = size(cir);
for i = 1:N_mpath
    dod = sin(angle(i,3)).*sin(angle(i,4));
    H = H + sqrt(cir(i,1))*exp(-1j*cir(i,2))*exp(-j*2*pi*d*dod*(0:N_BP-1).');
end
perform = abs(fft(H,N_beam));
[~,N] = max(perform);
end

