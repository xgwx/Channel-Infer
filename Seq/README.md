# Time-sequence channel inference 
Environment requirements:
1. Matlab
2. Python 3.X, Installed packages: numpy,scipy,jupyter notebook,sklearn,matplotlib,tensorflow(version suggests to be not lower than 1.5.1, lower versions may not work for lack of some functions).
Currently we use matlab to process data and show the results, python for Neural Network parts. The matlab part will be rewrited using python in future.  

Steps:
1. Extract the original data files in ../Data/HO.
2. Run MIMO_Channel_Generate.m in matlab. Modify the variables in the file to generate the CSI of the Target BS and the source BS.
3. Run HO_path_generator.m in matlab to generate the trace of a vehicle handover between two road site units.
4. Run Time_Sequence_train.py(or Time_Sequence_train.ipynb in jupyter notebook) with python to train the model.
5. Run Time_Sequence_infer.py(or Time_Sequence_infer.ipynb in jupyter notebook) with python to evaluate the performance of the model.
6. Run RNN_Performance_Analysis.m in matlab to evaluate the normalize beamforming loss and the average spectral effiency. The performance of location-based algorithm can be obtained by running LO_Performance_Analysis.m

