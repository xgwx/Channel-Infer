# import packages
import os
import math
import time
import json
import random
import scipy.io as sio

from collections import OrderedDict

import numpy as np
import tensorflow as tf

from seq2seq_model import Seq2SeqModel

# Loading data
Data = sio.loadmat('./Data/WI_MIMO_real_28000MHz_HO_tt_Beam256_sp7.mat')
X_train = Data['X_train']
y_train = Data['y_train']
X_test = Data['X_test']
y_test = Data['y_test']

# Decoding parameters
test_config = {}
# Network parameters
test_config['cell_type'] = 'lstm'
test_config['attention_type'] =  'luong'
test_config['hidden_units'] = 256
test_config['depth'] =  2
test_config['embedding_size'] =  100
test_config['num_encoder_symbols'] = 10001
test_config['num_decoder_symbols'] = 258
test_config['input_size'] = 32

test_config['use_residual'] =  False
test_config['attn_input_feeding'] =  False
test_config['use_dropout'] =  True
test_config['dropout_rate'] =  0.0
test_config['beam_width'] =  1
test_config['decode_batch_size'] =  128
test_config['max_decode_step'] = 1000
test_config['write_n_best'] =  False
test_config['model_path'] = '/Model/model_i50o50_h256_2_e100_ttbs_atth_sp7/'
test_config['decode_input'] = 'data/seq_channel_learning'
test_config['decode_output'] = './Result/' + test_config['model_path']
test_config['model_file'] = test_config['model_path'] + 'scl.ckpt-8400'
# Training parameters
test_config['learning_rate'] = 0.0002
test_config['max_gradient_norm'] =  1.0
test_config['batch_size'] =  128
test_config['max_epochs'] = 100
test_config['max_batch'] = 200000
test_config['max_load_batches'] = 20
test_config['max_seq_length'] = 50
test_config['display_freq'] = 100
test_config['save_freq'] =  11500
test_config['valid_freq'] = 1150000
test_config['optimizer'] = 'adam'
test_config['model_name'] = 'translate.ckpt'
test_config['shuffle_each_epoch'] = True
test_config['sort_by_length'] = True
test_config['use_fp16'] = False

# Runtime parameters
test_config['allow_soft_placement'] = True
test_config['log_device_placement'] = False

# Channel Inference parameters
source_len = 50
target_len = 50
path_tic = 0
loc_tic = 0

def load_model(session, test_config):
    model = Seq2SeqModel(test_config, 'decode')
    ckpt = tf.train.get_checkpoint_state(test_config['model_path'])
    if ckpt and tf.train.checkpoint_exists(ckpt.model_checkpoint_path):
        print ('Reloading model parameters..')
        # model.restore(session, ckpt.model_checkpoint_path)
        model.restore(session, test_config['model_file'])
    else:
        raise ValueError(
            'No such file:[{}]'.format(test_config['model_path']))
    return model

# without using embedding layers for input
def prepare_train_batch_new(X,y,batch_size,ptic,ltic):
    N_path = np.shape(X)[1]
    N_BS = np.shape(X[0,0])[1]
    flags = 0
    flags2 = 0
    X_batch = np.zeros([batch_size,source_len,N_BS],dtype=np.float32)
    y_batch = np.zeros([batch_size,target_len],dtype=np.int32)
    loc_batch = np.zeros([batch_size,2],dtype=np.int32)
    if path_tic >= N_path:
            flags = 1
    else:
        Temp_X = X[0,ptic]
        # Temp_X.shape = -1
        Temp_y = y[0,ptic]
        # Temp_y.shape = -1
        for i in range(batch_size):
            N_sam = np.shape(Temp_X)[0]
            while (ltic + source_len + target_len) >= N_sam:
                ptic = ptic + 1
                if(ptic >= N_path):
                    if i==0:
                        flags = 1
                    else:
                        flags2 = i
                    break
                else:
                    ltic = 0
                    Temp_X = X[0,ptic]
                    Temp_y = y[0,ptic]
                    N_sam = np.shape(Temp_X)[0]
            if(flags==1 or flags2>0):
                break
            else:
                X_batch[i,:,:] = np.array(Temp_X[ltic:ltic+source_len,:])
                y_batch[i,:] = np.array(Temp_y[ltic+source_len:ltic+source_len+target_len]).reshape(-1)
                loc_batch[i,0] = ptic
                loc_batch[i,1] = ltic+source_len
                ltic = ltic + source_len
    if flags == 1:
        return None,None,None,None,0,0,0
    if flags2>0: #last data samples
        X_batch = X_batch[0:flags2,:]
        y_batch = y_batch[0:flags2,:]
        loc_batch = loc_batch[0:flags2,:]
        X_length = [source_len for k in range(flags2)]       
        y_length = [target_len for k in range(flags2)]
    else:
        X_length = [source_len for k in range(batch_size)]       
        y_length = [target_len for k in range(batch_size)]
    X_length = np.array(X_length)
    y_length = np.array(y_length)
    y_batch = y_batch+1
    return X_batch,X_length,y_batch,y_length,ptic,ltic,loc_batch

path_tic = 0
loc_tic = 0
# creating output data
if not os.path.exists(test_config['decode_output']):
    os.makedirs(test_config['decode_output'])
tf.reset_default_graph()
# Initiate TF session
def angle_dis(a,b,N):
    a1 = -2*(a-1)
    a1 = a1 + 2*N*(a1<-N)
    b1 = -2*(b-1)
    b1 = b1 + 2*N*(b1<-N)
    c = abs(math.acos(a1/N)-math.acos(b1/N))
    return c

def angle_mse(a,b,N):
    a1 = -2*(a-2)
    a1 = a1 + 2*N*(a1<-N)
    b1 = -2*(b-2)
    b1 = b1 + 2*N*(b1<-N)
    c = abs(math.acos(a1/N)-math.acos(b1/N))
    c = c*c
    return c

with tf.Session(config=tf.ConfigProto(allow_soft_placement=test_config['allow_soft_placement'], 
    log_device_placement=test_config['log_device_placement'], gpu_options=tf.GPUOptions(allow_growth=True))) as sess:
    
    # Reload existing checkpoint
    model = load_model(sess, test_config)
    print('Decoding {}..'.format(test_config['decode_input']))
    dis = 0
    num = 0
    dis1 = np.zeros(target_len,dtype=np.float32)
    num1 = 0
    for i_batch in range(test_config['max_batch']):
    #for i_batch in range(1):
        path_tic_o = path_tic
        loc_tic_o = loc_tic
        source, source_len_g, target, target_len_g,path_tic,loc_tic,loc_batch = prepare_train_batch_new(X_test,y_test,test_config['decode_batch_size'],path_tic,loc_tic)
        # predicted_ids: GreedyDecoder; [batch_size, max_time_step, 1]
        # BeamSearchDecoder; [batch_size, max_time_step, beam_width]
        if source is None or target is None:
            print('No samples')
            break
        predicted_ids = model.predict(sess, encoder_inputs=source, 
                                      encoder_inputs_length=source_len_g)
        shape_target = np.shape(target)
        shape_predict = np.shape(predicted_ids)
        predicted = predicted_ids.reshape(shape_predict[0:2])
        predicted = predicted[:,0:np.shape(target)[1]]
        for i in range(np.shape(target)[0]):
            for j in range(0,np.shape(target)[1]):
                if(predicted[i,j]==1 and j > 0):
                    predicted[i,j] = predicted[i,j-1]
                dis  = dis + angle_mse(predicted[i,j],target[i,j],256)
                dis1[j] = dis1[j] + angle_mse(predicted[i,j],target[i,j],256)
        num = num + np.shape(target)[0]*np.shape(target)[1]
        num1 = num1 + np.shape(target)[0]
        np.savez(test_config['decode_output']+str(i_batch),results = predicted, loc_batch = loc_batch,target = target)
    dis_mse = math.sqrt(dis/num)
    dis1_mse = np.zeros(target_len,dtype=np.float32)
    for i_deruation in range(target_len):
        dis1_mse[i_deruation] = math.sqrt(dis1[i_deruation]/num1)
    print('Decoding terminated')
    print('RMSE of angle:',dis_mse)
    print('RMSE of angle per ts:', dis1_mse)

#Import output to csv files
import pandas as pd
model_path = 'model_i50o50_h256_2_e100_ttbs_atth_sp7/'
output_path = './Result/' + model_path
pa = []
piloc = []
predict = []
target = []
for i in range(24):
    data = np.load(output_path+str(i)+'.npz')
    pred_len = np.shape(data['results'])[1]
    predict_tmp = np.reshape(data['results'],-1)
    target_tmp = np.reshape(data['target'],-1)
    predict = np.concatenate((predict,predict_tmp))
    target = np.concatenate((target,target_tmp))
    loc_tmp = data['loc_batch']
    for j in range(np.shape(loc_tmp)[0]):
        pa = np.concatenate((pa,loc_tmp[j,0]*np.ones(pred_len)))
        piloc = np.concatenate((piloc,loc_tmp[j,1]+[k for k in range(1,pred_len+1)]))
target = target - 1
predict = predict - 1
dataframe = pd.DataFrame({'path':pa,'index':piloc,'predicted':predict,'target':target})
dataframe.to_csv(output_path+"result.csv",index=False,sep=',')