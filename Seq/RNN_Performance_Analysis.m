% Performance evaluation of the proposed Seq2seq framework
% Loading Path Data and Channel Data 
load('WI_MIMO_real_28000MHz_HO_tt_Beam256_sp7.mat');
load('WI_MIMO_Data_Para_TBS_HO_28000MHz.mat');
N_path = 601;
% Setting of TBS antennas and beam codebook size
Beam_Norm = 256;
Beam_true = 32;
N_point = 201;
predict_len = 50;
% Loading Results from Infer
result_file = '.\Result\model_i50o50_h256_2_e100_ttbs_atth_sp7\result.csv';
result = csvread(result_file,1,0);
[N_sam,~] = size(result);
Norm_perform = zeros(1,N_sam);
Norm_perform1 = zeros(1,N_sam);
N_train_path = length(X_train);
Per_bps = zeros(1,predict_len);
Per_bps1 = zeros(1,predict_len);
dis_num = zeros(1,predict_len);
P = 1;
N = 1e-12;
for i = 1:N_sam
    path_id = round(result(i,2)+1)+N_train_path;
    loc_tmp = loc{path_id};
    i_loc = (loc_tmp(1,2)-1)*N_point + loc_tmp(round(result(i,1)),1);
    H1 = BeamResponse_gscm_New(cir_t{i_loc},angle_t{i_loc},Beam_true,Beam_Norm,0.5);
    tmp_indx1 = round(result(i,3)*Beam_Norm/256);
    %tmp_indx1 = tmp_indx1+Beam_Norm*(tmp_indx1==0);
    Norm_perform(i) = H1(tmp_indx1)/max(H1);  %Result of the inferred output
    tmp_indx2 = round(result(i,4)*Beam_Norm/256);
    %tmp_indx2 = tmp_indx2+Beam_Norm*(tmp_indx2==0); %Result of the target output
    Norm_perform1(i) = H1(tmp_indx2)/max(H1);
    dis = mod(round(result(i,1)),predict_len)+1;
    Per_bps(1,dis) = Per_bps(1,dis) + log2(1+P*(H1(tmp_indx1))^2/Beam_true/N);
    Per_bps1(1,dis) = Per_bps1(1,dis) + log2(1+P*(H1(tmp_indx2))^2/Beam_true/N);
    dis_num(1,dis) = dis_num(1,dis) + 1;
end
% The average spectral efficiency
Per_bps = Per_bps./dis_num; %Result of the inferred output
Per_bps1 = Per_bps1./dis_num; %Result of the target output
figure; cdfplot(Norm_perform);hold on; cdfplot(Norm_perform1);