function [N] = BeamResponse_gscm(cir,angle,N_BP,d)
%BeamResponse_gscm Generating the beam response (DFT domain) based on GSCM, UEs with single antennas
H = zeros(N_BP,1);
[N_mpath,~] = size(cir);
for i = 1:N_mpath
    dod = sin(angle(i,3)).*sin(angle(i,4));
    H = H + sqrt(cir(i,1))*exp(-1j*cir(i,2))*exp(-j*2*pi*d*dod*(0:N_BP-1).');
end
perform = abs(fft(H));
N = perform;
%[~,N] = max(perform);
%N = perform.^2/sum(perform.^2);
%N = exp(N)/sum(exp(N));
%N = exp(perform)/sum(exp(perform));
end

