# import packages
import os
import math
import time
import json
import random
import scipy.io as sio

from collections import OrderedDict

import numpy as np
import tensorflow as tf

from seq2seq_model import Seq2SeqModel

# Loading data
Data = sio.loadmat('./Data/WI_MIMO_real_28000MHz_HO_tt_Beam256_sp7.mat')
X_train = Data['X_train']
y_train = Data['y_train']
X_test = Data['X_test']
y_test = Data['y_test']

# parameters setting
train_config = {}
train_config['source_vocabulary'] = ''
train_config['target_vocabulary'] = ''
train_config['source_train_data'] = ''
train_config['target_train_data'] = ''
train_config['source_valid_data'] =  ''
train_config['target_valid_data'] =  ''

# Network parameters
train_config['input_size'] = 32
train_config['cell_type'] = 'lstm'
train_config['attention_type'] =  'luong'
train_config['hidden_units'] = 256
train_config['depth'] =  2
train_config['embedding_size'] =  500
train_config['num_encoder_symbols'] = 10001
train_config['num_decoder_symbols'] = 258

train_config['use_residual'] =  False
train_config['attn_input_feeding'] =  True
train_config['use_dropout'] =  True
train_config['dropout_rate'] =  0.5

# Training parameters
train_config['learning_rate'] = 0.0002
train_config['max_gradient_norm'] =  1.0
train_config['batch_size'] =  128
train_config['max_epochs'] = 100
train_config['max_batch'] = 10000
train_config['max_load_batches'] = 20
train_config['max_seq_length'] = 50
train_config['display_freq'] = 100
train_config['save_freq'] =  11500
train_config['valid_freq'] = 1150000
train_config['optimizer'] = 'adam'
train_config['model_dir'] =  './Model/model_i50o50_h256_2_e500_ttbs_atth_sp7/'
train_config['summary_dir'] = train_config['model_dir']+'/summary'
train_config['model_name'] = 'scl.ckpt'
train_config['shuffle_each_epoch'] = True
train_config['sort_by_length'] = True
train_config['use_fp16'] = False

# Runtime parameters
train_config['allow_soft_placement'] =  True
train_config['log_device_placement'] = False

# Channel Inference parameters
source_len = 50
target_len = 50
path_tic = 0
loc_tic = 0
model_load = True

# create model
def create_model(session, train_config):

    model = Seq2SeqModel(train_config, 'train')

    ckpt = tf.train.get_checkpoint_state(train_config['model_dir'])
    if ckpt and model_load and tf.train.checkpoint_exists(ckpt.model_checkpoint_path):
        print('Reloading model parameters..')
        model.restore(session, ckpt.model_checkpoint_path)
        
    else:
        if not os.path.exists(train_config['model_dir']):
            os.makedirs(train_config['model_dir'])
        print('Created new model parameters..')
        session.run(tf.global_variables_initializer())
   
    return model

def prepare_train_batch_new(X,y,batch_size,ptic,ltic):
    N_path = np.shape(X)[1]
    N_BS = np.shape(X[0,0])[1]
    flags = 0
    flags2 = 0
    X_batch = np.zeros([batch_size,source_len,N_BS],dtype=np.float32)
    y_batch = np.zeros([batch_size,target_len],dtype=np.int32)
    if path_tic >= N_path:
            flags = 1
    else:
        Temp_X = X[0,ptic]
        # Temp_X.shape = -1
        Temp_y = y[0,ptic]
        # Temp_y.shape = -1
        for i in range(batch_size):
            N_sam = np.shape(Temp_X)[0]
            while (ltic + source_len + target_len) >= N_sam:
                ptic = ptic + 1
                if(ptic >= N_path):
                    if i==0:
                        flags = 1
                    else:
                        flags2 = i
                    break
                else:
                    ltic = 0
                    Temp_X = X[0,ptic]
                    Temp_y = y[0,ptic]
                    N_sam = np.shape(Temp_X)[0]
            if(flags==1 or flags2>0):
                break
            else:
                X_batch[i,:,:] = np.array(Temp_X[ltic:ltic+source_len,:])
                y_batch[i,:] = np.array(Temp_y[ltic+source_len:ltic+source_len+target_len]).reshape(-1)
                ltic = ltic + target_len
    if flags == 1:
        return None,None,None,None,0,0
    if flags2>0:           #last data samples
        X_batch = X_batch[0:flags2,:]
        y_batch = y_batch[0:flags2,:]
        X_length = [source_len for k in range(flags2)]       
        y_length = [target_len for k in range(flags2)]  
    else:
        X_length = [source_len for k in range(batch_size)]       
        y_length = [target_len for k in range(batch_size)]
    X_length = np.array(X_length)
    y_length = np.array(y_length)
    y_batch = y_batch+1
    return X_batch,X_length,y_batch,y_length,ptic,ltic

# Load parallel data to train
print('Loading training data..')
if train_config['source_valid_data'] and train_config['source_valid_data']:
    print('Loading validation data..')
else:
    valid_set = None
tf.reset_default_graph()
# Initiate TF session
with tf.Session(config=tf.ConfigProto(allow_soft_placement=train_config['allow_soft_placement'], 
    log_device_placement=train_config['log_device_placement'], gpu_options=tf.GPUOptions(allow_growth=True))) as sess:

    # Create a log writer object
    log_writer = tf.summary.FileWriter(train_config['model_dir'], graph=sess.graph)

    # Create a new model or reload existing checkpoint
    model = create_model(sess, train_config)
    step_time, loss = 0.0, 0.0
    words_seen, sents_seen = 0, 0
    start_time = time.time()

    # Training loop
    print('Training..')
    for epoch_idx in range(train_config['max_epochs']):
        if model.global_epoch_step.eval() >= train_config['max_epochs']:
            print('Training is already complete.', \
                  'current epoch:{}, max epoch:{}'.format(model.global_epoch_step.eval(), train_config['max_epochs']))
            break
        #reset data
        path_tic = 0
        loc_tic = 0
        for i_batch in range(train_config['max_batch']):    
            # Get a batch from training parallel data
            source, source_len_g, target, target_len_g,path_tic,loc_tic = prepare_train_batch_new(X_train,y_train,train_config['batch_size'],path_tic,loc_tic)
            if source is None or target is None:
                print('No samples under max_seq_length ', train_config['max_seq_length'])
                break

            # Execute a single training step
            step_loss, summary = model.train(sess, encoder_inputs=source, encoder_inputs_length=source_len_g, 
                                             decoder_inputs=target, decoder_inputs_length=target_len_g)

            loss += float(step_loss) / train_config['display_freq']
            words_seen += float(np.sum(source_len_g+target_len_g))
            sents_seen += float(source.shape[0]) # batch_size

            if model.global_step.eval() % train_config['display_freq'] == 0:

                avg_perplexity = math.exp(float(loss)) if loss < 300 else float("inf")

                time_elapsed = time.time() - start_time
                step_time = time_elapsed / train_config['display_freq']

                words_per_sec = words_seen / time_elapsed
                sents_per_sec = sents_seen / time_elapsed

                print('Epoch ', model.global_epoch_step.eval(), 'Step ', model.global_step.eval(), \
                      'Perplexity {0:.2f}'.format(avg_perplexity), 'Step-time ', step_time, \
                      '{0:.2f} pieces/s'.format(sents_per_sec), '{0:.2f} points/s'.format(words_per_sec))

                loss = 0
                words_seen = 0
                sents_seen = 0
                start_time = time.time()

                # Record training summary for the current batch
                log_writer.add_summary(summary, model.global_step.eval())

            # Execute a validation step
            if valid_set and model.global_step.eval() % train_config['valid_freq'] == 0:
                print('Validation step')
                valid_loss = 0.0
                valid_sents_seen = 0
                for source_seq, target_seq in valid_set:
                    # Get a batch from validation parallel data
                    source, source_len, target, target_len = prepare_train_batch(source_seq, target_seq)

                    # Compute validation loss: average per word cross entropy loss
                    step_loss = model.eval(sess, encoder_inputs=source, encoder_inputs_length=source_len_g,
                                           decoder_inputs=target, decoder_inputs_length=target_len_g)
                    batch_size = source.shape[0]

                    valid_loss += step_loss * batch_size
                    valid_sents_seen += batch_size
                    print(' {} samples seen'.format(valid_sents_seen))

                valid_loss = valid_loss / valid_sents_seen
                print('Valid perplexity: {0:.2f}'.format(math.exp(valid_loss)))  

        # Increase the epoch index of the model
        model.global_epoch_step_op.eval()
        print('Epoch {0:} DONE'.format(model.global_epoch_step.eval()))
        if(model.global_epoch_step.eval()%10==0):
            print('Saving the model..')
            checkpoint_path = os.path.join(train_config['model_dir'], train_config['model_name'])
            model.save(sess, checkpoint_path, global_step=model.global_step)
            json.dump(model.config,
                      open('%s-%d.json' % (checkpoint_path, model.global_step.eval()), 'w'),indent=2)

print('Training Terminated')