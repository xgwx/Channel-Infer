%Normalized beamforming loss
load('./Result/BD_M1T1_C128T32U8_2600MHz_htest.mat');
ytp = csvread('./Result/BD_M1T1_C128T32U8_2600MHz_beam256_ytp.csv',1,0);
ytp = ytp+1;
yrp = csvread('./Result/BD_M1T1_C128T32U8_2600MHz_neam256_yrp.csv',1,0);
yrp = yrp+1;
tp_code = 256;
tp_ant = 32;
rp_ant = 8;
rp_code = 8;
norm_ytp = zeros(size(ytp));
norm_yrp = zeros(size(ytp));
norm_ytrp = zeros(size(ytp));
for i = 1:length(ytp)
    H_real = reshape(H_test(i,:),tp_ant,rp_ant);
    H_res = abs(fft2(H_real,tp_code,rp_code));
    temp_max = max(max(H_res));
    tp_indx = ytp(i);
    rp_indx = yrp(i);
    norm_ytp(i) = max(H_res(tp_indx,:))/temp_max;
    norm_yrp(i) = max(H_res(:,rp_indx))/temp_max;
    norm_ytrp(i) = H_res(tp_indx,rp_indx)/temp_max;
end
figure;hold on;
cdfplot(1-norm_ytp);
cdfplot(1-norm_yrp);
cdfplot(1-norm_ytrp);