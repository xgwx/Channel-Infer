#Remote beamforming inference 
Environment requirements:
1. Matlab
2. Python 3.X, Installed packages: numpy,scipy,jupyter notebook,sklearn,matplotlib,tensorflow(version suggests to be not lower than 1.0.0, lower versions may not work for lack of some functions).
Currently we use matlab to process data and show the results, python for Neural Network parts. The matlab part will be rewrited using python in future.  

Steps:
1. Extract the original data files in ../Data/HCN.
2. Run Channel_Parameter_Loading.m in matlab. Modify the variables in the file to generate the CSI of the Macro BS and the traffic BS.
3. Run BD_Generator.m in matlab to generate the csi of CBS and the beam direction of TBS. Antennas and beam codebook sizes are variable.
4. Run BeamDirectionTx.py(or BeamDirectionTx.ipynb in jupyter notebook) with python to train the transmit precoding.
5. Run BeamDirectionRx.py(or BeamDirectionRx.ipynb in jupyter notebook) with python to train and receive precoding.
6. Run Perf_NormBeamloss.m in matlab to evaluate the performance of normalized beamforming loss.
7. Run Perf_SpecEff.m in matlab to evaluate the performance of achievable rate. (Perf_SpecEff_loc.m stands for the results from location based algorithm)

