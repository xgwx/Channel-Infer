% Performance of location based beamforming.
L_t = [649.577,667.879,41.5285];
load('.\Data\BD_M1T1_C128T32U8_2600MHz.mat')
load('.\Data\BD_infer_loc');
[N_MS,~] = size(LocData);
%Loc_perform = zeros(1,N_MS);
loc_err_perform = zeros(3,N_MS);
loc_bps_perform = zeros(3,N_MS);
tp_code = 256;
tp_ant = 32;
rp_ant = 8;
rp_code = 8;
spef_ytp = 0;
spef_yrp = 0;
spef_ytrp = 0;
spef_max = 0;
P = 1;
N = 1e-12;
N_sam = 100;
loc_error = [1,0.1,0];
for i = 1:N_MS
    H_real = reshape(H_TBS(i,:),tp_ant,rp_ant);
    H_res = abs(fft2(H_real,tp_code,rp_code));
    for i_sp = 1:N_sam
        for i_err = 1:length(loc_error)
            temp_vec = LocData(i,:) - L_t + 1/sqrt(2)*loc_error(i_err)*(randn(1)*[1,0,0]+randn(1)*[0,1,0]);
            cos_dod = temp_vec(2)/norm(temp_vec);
            cos_doa = -temp_vec(2)/norm(temp_vec);
            %cos_th2(i) = sin(angle_t{i}(1,3))*sin(angle_t{i}(1,4));
            H_loc = exp(-1j*2*pi*0.5*cos_dod*(0:tp_ant-1).')*exp(-1j*2*pi*0.5*cos_doa*(0:rp_ant-1));
            H_resloc = abs(fft2(H_loc,tp_code,rp_code));
            [H_reslocmtp,tp_max] = max(H_resloc,[],1);
            [~,loc_rp] = max(H_reslocmtp);
            loc_tp = tp_max(loc_rp);
            loc_err_perform(i_err,i) = loc_err_perform(i_err,i) + H_res(loc_tp,loc_rp)/max(max(H_res));
            loc_bps_perform(i_err,i) = loc_bps_perform(i_err,i) + log2(1+P*(H_res(loc_tp,loc_rp))^2/tp_ant/rp_ant/N);
            %Loc_perform(i) = H1(y_loc(i))/max(H1); 
        end
    end
end
loc_err_perform = loc_err_perform/N_sam;
loc_bps_perform = loc_bps_perform/N_sam;
save('loc_BD_M1T1_C128T32U8_2600MHz.mat','loc_err_perform','loc_error','loc_bps_perform');
%save('Loc_Perfect.mat','y_loc','cos_th');
%loc_perfect = Loc_perform;
%save('loc_perform28GHz_beam32.mat','loc_err_perform','loc_error');