function [H] = CSI_gscm(cir,angle,N_BS,N_UE,d)
%CSI_GSCM Generating CSI based on��GSCM
H = zeros(N_BS,N_UE);
[N_mpath,~] = size(cir);
for i = 1:N_mpath
    dod = sin(angle(i,3)).*sin(angle(i,4));
    doa = sin(angle(i,1)).*sin(angle(i,2));
    H = H + sqrt(cir(i,1))*exp(-1j*cir(i,2))*exp(-1j*2*pi*d*dod*(0:N_BS-1).')*exp(-1j*2*pi*d*doa*(0:N_UE-1));
end
end

