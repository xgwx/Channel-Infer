%Average spectral efficiency of the NN-based beamforming inference
load('./Result/BD_M1T1_C64T32U8_2600MHz_htest.mat');
ytp = csvread('./Result/BD_M1T1_C64T32U8_2600MHz_beam32_ytp.csv',1,0);
ytp = ytp+1;
yrp = csvread('./Result/BD_M1T1_C64T32U8_2600MHz_yrp.csv',1,0);
yrp = yrp+1;
tp_code = 32;
tp_ant = 32;
rp_ant = 8;
rp_code = 8;
spef_ytp = 0;
spef_yrp = 0;
spef_ytrp = 0;
spef_max = 0;
P = 1;
N = 1e-10;
for i = 1:length(ytp)
    H_real = reshape(H_test(i,:),tp_ant,rp_ant);
    H_res = abs(fft2(H_real,tp_code,rp_code));
    tp_indx = ytp(i);
    rp_indx = yrp(i);
    spef_ytp = spef_ytp +  log2(1+P*(max(H_res(tp_indx,:)))^2/tp_ant/rp_ant/N);
    spef_yrp = spef_yrp +  log2(1+P*(max(H_res(:,rp_indx)))^2/tp_ant/rp_ant/N);
    spef_ytrp = spef_ytrp + log2(1+P*(H_res(tp_indx,rp_indx))^2/tp_ant/rp_ant/N);
    spef_max = spef_max + log2(1+P*(max(max(H_res)))^2/tp_ant/rp_ant/N);
end
spef_ytp = spef_ytp/length(ytp);
spef_yrp = spef_yrp/length(ytp);
spef_ytrp = spef_ytrp/length(ytp);
spef_max = spef_max/length(ytp);
save('./Result/spec_C64T32U8B32.mat','spef_ytp','spef_yrp','spef_ytrp','spef_max');
%figure; cdfplot(Norm_perform);hold on; cdfplot(Norm_perform1);