# Receive beamforming inference
#Import package and data
import tensorflow as tf
import numpy as np
import os
import math
import scipy.io as sio
from sklearn.model_selection import train_test_split
from matplotlib import pyplot
import matplotlib.mlab as mlab
%matplotlib inline

Data = sio.loadmat('./Data/BD_M1T1_C64T32U8_2600MHz.mat')
X= Data['X']
y = Data['y_r']
H_TBS = Data['H_TBS']

def onehot_encode(y):
    N_UE = np.shape(y)[0]
    N_BS = np.max(y)
    y_e = np.zeros([N_UE,N_BS])
    for i in range(N_UE):
        y_e[i,y[i]-1] = 1
    return y_e

y = onehot_encode(y)
y_con = np.concatenate((y, H_TBS), axis=1)
X_train, X_test, Y_train_c, Y_test_c = train_test_split(X,y_con,test_size=0.2, random_state=16)
y_train = np.real(Y_train_c[:,0:np.shape(y)[1]])
y_test = np.real(Y_test_c[:,0:np.shape(y)[1]])
h_test = Y_test_c[:,np.shape(y)[1]:]
Total_train = np.concatenate((X_train, y_train), axis=1)
def next_batch_train():
    np.random.shuffle(Total_train)
    y = Total_train[0:100, np.shape(X_train)[1]:]
    X = Total_train[0:100, 0:np.shape(X_train)[1]]
    return X, y

loss_rate_train = 0
hidden_layer = [256,128,64,32]
batch_size = 128
hl_num = np.shape(hidden_layer)[0]
tl_num = 3
# Small epsilon value for the BN transform
bn_epsilon = 1e-3
is_transfer = False
if is_transfer:
    Data1= np.load('./model/ms3_weights.npz')
    w = {}
    w[chr(0)] = Data1['W_1']
    w[chr(1)] = Data1['W_2']
    w[chr(2)] = Data1['W_3']
    w['f'] = np.random.normal(size=(hidden_layer[-1], np.shape(y_train)[1])).astype(np.float32)
    w['b'] = np.random.normal(size=(np.shape(y_train)[1])).astype(np.float32)+0.1
    # w['f'] = Data1['W_f']
    # w['b'] = Data1['b_f']
else:
    w = {}
    w[chr(0)] = np.random.normal(size=(np.shape(X_train)[1], hidden_layer[0])).astype(np.float32)
    for i in range(1,hl_num):
        w[chr(i)] = np.random.normal(size=(hidden_layer[i-1], hidden_layer[i])).astype(np.float32)
    w['f'] = np.random.normal(size=(hidden_layer[-1], np.shape(y_train)[1])).astype(np.float32)
    w['b'] = np.random.normal(size=(np.shape(y_train)[1])).astype(np.float32)+0.1
    
        
def next_batch_train():
    np.random.shuffle(Total_train)
    y = Total_train[0:batch_size, np.shape(X_train)[1]:]
    X = Total_train[0:batch_size, 0:np.shape(X_train)[1]]
    return X, y

def add_bnlayer(X,W,h_n):
    z_bn = tf.matmul(X,W)
    batch_mean, batch_var = tf.nn.moments(z_bn,[0])
    scale = tf.Variable(tf.ones([h_n]))
    beta = tf.Variable(tf.zeros([h_n]))
    bn = tf.nn.batch_normalization(z_bn,batch_mean,batch_var,beta,scale,bn_epsilon)
    bn = tf.nn.dropout(bn,keep_prob)
    output = tf.nn.relu(bn)
    return output

tf.reset_default_graph()

with tf.name_scope('keep_prob'):
    keep_prob = tf.placeholder(tf.float32)

with tf.name_scope('learning_rate'):
    l_rate = tf.placeholder(tf.float32)
    
with tf.name_scope('input_layer'):
    x_train = tf.placeholder(tf.float32, shape=[None, np.shape(X_train)[1]], name='x_input')
    y_label_train = tf.placeholder(tf.float32, shape=[None, np.shape(y_train)[1]], name='y_input')

W_1 = tf.Variable(w[chr(0)])
output_1 = add_bnlayer(x_train,W_1,hidden_layer[0])
W_2 = tf.Variable(w[chr(1)])
output_2 = add_bnlayer(output_1,W_2,hidden_layer[1])
W_3 = tf.Variable(w[chr(2)])
output_3 = add_bnlayer(output_2,W_3,hidden_layer[2])
W_4 = tf.Variable(w[chr(3)])
output_4 = add_bnlayer(output_3,W_4,hidden_layer[3])

with tf.name_scope('output_layer1'):
    with tf.name_scope('Weight'):
        W_f = tf.Variable(w['f'], name='W_f')
        tf.summary.histogram('output_layer/weight', W_f)
    with tf.name_scope('bias'):
        b_f = tf.Variable(w['b'], name='b_f')
        tf.summary.histogram('output_layer/bias', b_f)
    with tf.name_scope('Wx_plus_b'):
        Wx_plus_b_f = tf.matmul(output_4, W_f) + b_f
        tf.summary.histogram('output_layer/Wx_plus_b_f', Wx_plus_b_f)
y_result_train = Wx_plus_b_f
regularizer = tf.nn.l2_loss(W_f)

with tf.name_scope('cross_entropy'):
    cross_entropy_train = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(labels=y_label_train, logits=y_result_train))

with tf.name_scope('loss'):
    loss_train = cross_entropy_train

with tf.name_scope('train'):
    train_step_train = tf.train.AdamOptimizer(learning_rate=l_rate, epsilon=1e-8).minimize(loss_train)
    # train_step_train = tf.train.GradientDescentOptimizer(learning_rate=0.01).minimize(loss_train)

gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.5)
correct_prediction = tf.equal(tf.argmax(y_result_train, 1), tf.argmax(y_label_train, 1))
with tf.name_scope('Accuracy'):
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# saver = tf.train.Saver()
# saver = tf.train.import_meta_graph("./model/c1t1_model.meta")
init = tf.global_variables_initializer()

sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
##save
model_dir = './Model/BD_M1T1_C64T32U8_2600MHz_RB1/'
model_reload = True
saver = tf.train.Saver()
ckpt = tf.train.get_checkpoint_state(model_dir)
if ckpt and model_reload and tf.train.checkpoint_exists(ckpt.model_checkpoint_path):
    print('Reloading model parameters..')
    saver.restore(sess, save_path=ckpt.model_checkpoint_path)   
else:
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    print('Created new model parameters..')
    sess.run(init)

train_ori1 = np.zeros(2000)
test_ori1 = np.zeros(2000)
index = 0
max_lerate = 0.03
min_lerate = 0.0005
decay = 10000
saver = tf.train.Saver()
for i in range(20000):
    lerate = min_lerate + (max_lerate-min_lerate)*math.exp(-i/decay)
    batch_x_train, batch_y_train = next_batch_train()
    sess.run(train_step_train,
        feed_dict={x_train: batch_x_train, y_label_train: batch_y_train, keep_prob: 0.95, l_rate: lerate})
    if i % 10 == 0:
        test_ori1[index] = sess.run(accuracy, feed_dict={x_train: X_test, y_label_train: y_test, keep_prob: 1})
        train_ori1[index] = sess.run(accuracy, feed_dict={x_train: X_train, y_label_train: y_train, keep_prob: 1})
        index = index + 1
    if i%100 == 99:
        print('TBS1 Test_rate = %f' % (test_ori1[index-1]))
        print('TBS1 Train_rate = %f' % (train_ori1[index-1]))
        # lerate = lerate * decay

import pandas as pd
y_rp = sess.run(tf.argmax(y_result_train, 1), feed_dict={x_train: X_test, y_label_train: y_test, keep_prob: 1})
dataframe = pd.DataFrame({'predict_r':y_rp})
dataframe.to_csv('./Result/BD_M1T1_C64T32U8_2600MHz_yrp.csv',index=False,sep=',')
# sio.savemat('./Result/BD_M1T1_C64T32U8_2600MHz_yrp.mat',{'H_test':h_test})