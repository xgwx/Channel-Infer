function [N_T,N_R] = BeamPattern_gscm2(cir,angle,N_BP,N_UE,N_beamt,N_beamr,d)
%CSI_GSCM Generating CSI based on��GSCM
H = zeros(N_BP,N_UE);
[N_mpath,~] = size(cir);
for i = 1:N_mpath
    dod = sin(angle(i,3)).*sin(angle(i,4));
    doa = sin(angle(i,1)).*sin(angle(i,2));
    H = H + sqrt(cir(i,1))*exp(-1j*cir(i,2))*exp(-1j*2*pi*d*dod*(0:N_BP-1).')*exp(-1j*2*pi*d*doa*(0:N_UE-1));
end
perform = abs(fft2(H,N_beamt,N_beamr));
[temp_perform,temp_N] = max(perform,[],2);
[~,N_T] = max(temp_perform,[],1);
N_R = temp_N(N_T);
end

