%% Load Channel Parameters and generate the csi of MBS and the optimal beam directions of TBS
N_CBSA = 128;
N_TBSA = 64;
N_UEA = 8;
N_beamlist = [32,64,128,256,512,1024];
N_beamr = 8;
load('.\Data\WI_MIMO_Para_MBS_2600MHz.mat');
angle_c = angle;
cir_c = cir;
valid_c = valid;
load('.\Data\WI_MIMO_Para_TBS_2600MHz.mat');
angle_t = angle;
cir_t = cir;
valid_t = valid;
%load('HO_loc_data');
valid = logical(valid_c.*valid_t);
N_MS = length(valid);
X = zeros(N_MS,N_CBSA*N_UEA);
H_TBS = zeros(N_MS,N_TBSA*N_UEA);
y_t = zeros(N_MS,length(N_beamlist));
y_r = zeros(N_MS,1);
for i = 1:N_MS
    %generate raw csi data of CBS and TBS
    if(valid(i))
        cbs_tmp = CSI_gscm(cir_c{i},angle_c{i},N_CBSA,N_UEA,0.5);
        cbs_tmp = log10(abs(fft2(cbs_tmp)));
        X(i,:) = reshape(cbs_tmp,[1,N_CBSA*N_UEA]);
        tbs_tmp = CSI_gscm(cir_t{i},angle_t{i},N_TBSA,N_UEA,0.5);
        H_TBS(i,:) = reshape(tbs_tmp,[1,N_TBSA*N_UEA]);
        for j = 1:length(N_beamlist)
            [y_t(i,j),y_r(i)] = BeamPattern_gscm2(cir_t{i},angle_t{i},N_TBSA,N_UEA,N_beamlist(j),N_beamr,0.5);
        end       
    end
end

X = X(valid,:);
H_TBS = H_TBS(valid,:);
y_t = y_t(valid,:);
y_r = y_r(valid,:);
%save('.\Data\BD_M1T1_C256T32U8_beam32_2600MHz.mat','X','y_t','y_r','H_TBS','N_CBSA','N_TBSA','N_UEA','N_beamt','N_beamr');
save('.\Data\BD_M1T1_C128T64U8_2600MHz.mat','X','H_TBS','y_t','y_r','N_CBSA','N_TBSA','N_UEA','N_beamlist','N_beamr');
