%% Loading Parameters From Generated Data
StdPath = '..\Data\HCN\';
%UE sets: rxSet005
rxSet = 'rxSet005';
%MBS 1: rxSet001; MBS 2: rxSet002; TBS 1: rxSet003; TBS 2: rxSet004;
txSet = 'txSet004';
%% Load Locations
% LocFile = strcat(StdPath,'NewStart.txloss.t001_01.r006.p2m');
% LocData = distilldata(LocFile);
% LocData = LocData(:,2:3);
%% Load Channel Data
CirFile = dir([StdPath,'cir\*',txSet,'*',rxSet,'*.csv']);
N_UE1 = length(CirFile);
AoAFile = dir([StdPath,'doa\*',txSet,'*',rxSet,'*.csv']);
N_UE2 = length(AoAFile);
if(N_UE1~=N_UE2)
    print('users incorrect');
end
cir = cell(1,N_UE1);
angle = cell(1,N_UE1);
valid = ones(1,N_UE1);
for i = 1:N_UE1
    if(i>99)
        temp_cir_file = [StdPath,'cir\cir.',txSet,'.txPt001.',rxSet,'.rxPt',num2str(i),'.txEl001.rxEl001.inst001.csv'];
        temp_doa_file = [StdPath,'doa\angles.',txSet,'.txPt001.',rxSet,'.rxPt',num2str(i),'.txEl001.rxEl001.csv'];
    elseif(i>9)
        temp_cir_file = [StdPath,'cir\cir.',txSet,'.txPt001.',rxSet,'.rxPt0',num2str(i),'.txEl001.rxEl001.inst001.csv'];
        temp_doa_file = [StdPath,'doa\angles.',txSet,'.txPt001.',rxSet,'.rxPt0',num2str(i),'.txEl001.rxEl001.csv'];
    else
        temp_cir_file = [StdPath,'cir\cir.',txSet,'.txPt001.',rxSet,'.rxPt00',num2str(i),'.txEl001.rxEl001.inst001.csv'];
        temp_doa_file = [StdPath,'doa\angles.',txSet,'.txPt001.',rxSet,'.rxPt00',num2str(i),'.txEl001.rxEl001.csv'];
    end
fid = fopen(temp_cir_file,'rb'); 
fseek(fid,0,'eof'); 
fileSize = ftell(fid); 
fclose(fid); 
if(fileSize>75)
temp = csvread(temp_cir_file,1,0);
cir{1,i} = temp(:,3:5);
temp1 = csvread(temp_doa_file,1,0);
[npath,~] = size(temp1);
angle{1,i} = temp1(:,3:7);
else
    valid(i) = 0;
end
end
save('.\Data\WI_MIMO_Para_TBS2_2600MHz.mat','cir','angle','valid');