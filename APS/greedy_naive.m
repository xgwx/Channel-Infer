function [coloring,available_colors] = greedy_naive(T)
% Naive greedy
%   Detailed explanation goes here
n = size(T,1);
coloring = zeros(n,1);
available_colors = 0;

% Start with the node that has the maximum degree.
% Color the current node with the lowest available color.
% Select the next node by selecting the node with the maximum degree of saturation. 
% This means that you have to select the node that has the most number of unique neighboring colors. 
% In case of a tie, use the node with the largest degree.
% Goto step 2. until all nodes are colored.


for i = 1:n
    v = i;
    neighbor_colors = sort(unique(T(v,:).*coloring'));
    if neighbor_colors(1) == 0;
        neighbor_colors = neighbor_colors(2:end);
    end
    if length(neighbor_colors) == available_colors
        available_colors = available_colors+1;
        coloring(v) = available_colors;
    else
        c = 1;
        while(sum(neighbor_colors==c)>0)
            c = c+1;
        end
        coloring(v) = c;
    end
end

end

