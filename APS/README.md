# Angular Power Spectrum  inference 
Environment requirements:
1. Matlab
2. Python 3.X, Installed packages: numpy,scipy,jupyter notebook,sklearn,matplotlib,tensorflow(version suggests to be not lower than 1.0.0, lower versions may not work for lack of some functions).
Currently we use matlab to process data and show the results, python for Neural Network parts. The matlab part will be rewrited using python in future.  

Steps:
1. Extract the original data files in ../Data/HCN.
2. Run Channel_Parameter_Loading.m in matlab. Modify the variables in the file to generate the CSI of the Macro BS and the traffic BS.
3. Run APS_Generator.m in matlab to generate the csi of TBS and CBS.
4. Run aps_infer_CNN.py(or aps_infer_CNN.ipynb in jupyter notebook) with python to train and infer with CNN.
5. Run aps_infer_GAN.py(or aps_infer_GAN.ipynb in jupyter notebook) with python to train and infer with GAN.
6. Run pairing_TBS_K.m in matlab to group users with different settings of K.
7. Run pairing_TBS_SNR.m in matlab to group users with different settings of SNR.

