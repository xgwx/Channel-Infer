%% Loading Parameters From Generated Data
StdPath = '..\Data\HCN\';
%rxSet005: UE sets
rxSet = 'rxSet005';
%txSet001: Macro BS 1; txSet002: Macro BS 2; txSet003: Traffic BS(small cell) 1; txSet004:raffic BS(small cell) 4
txSet = 'txSet003';
CirFile = dir([StdPath,'cir\*',txSet,'*',rxSet,'*.csv']);
N_UE1 = length(CirFile);
AoAFile = dir([StdPath,'doa\*',txSet,'*',rxSet,'*.csv']);
N_UE2 = length(AoAFile);
if(N_UE1~=N_UE2)
    Disp('users incorrect');
end
cir = cell(1,N_UE1);
angle = cell(1,N_UE1);
valid = ones(1,N_UE1);
for i = 1:N_UE1
    if(i>99)
        temp_cir_file = [StdPath,'cir\cir.',txSet,'.txPt001.',rxSet,'.rxPt',num2str(i),'.txEl001.rxEl001.inst001.csv'];
        temp_doa_file = [StdPath,'doa\angles.',txSet,'.txPt001.',rxSet,'.rxPt',num2str(i),'.txEl001.rxEl001.csv'];
    elseif(i>9)
        temp_cir_file = [StdPath,'cir\cir.',txSet,'.txPt001.',rxSet,'.rxPt0',num2str(i),'.txEl001.rxEl001.inst001.csv'];
        temp_doa_file = [StdPath,'doa\angles.',txSet,'.txPt001.',rxSet,'.rxPt0',num2str(i),'.txEl001.rxEl001.csv'];
    else
        temp_cir_file = [StdPath,'cir\cir.',txSet,'.txPt001.',rxSet,'.rxPt00',num2str(i),'.txEl001.rxEl001.inst001.csv'];
        temp_doa_file = [StdPath,'doa\angles.',txSet,'.txPt001.',rxSet,'.rxPt00',num2str(i),'.txEl001.rxEl001.csv'];
    end
fid = fopen(temp_cir_file,'rb'); 
fseek(fid,0,'eof'); 
fileSize = ftell(fid); 
fclose(fid); 
if(fileSize>75)
temp = csvread(temp_cir_file,1,0);
cir{1,i} = temp(:,3:5);
temp1 = csvread(temp_doa_file,1,0);
[npath,~] = size(temp1);
angle{1,i} = temp1(:,3:7);
else
    valid(i) = 0;
end
end
save('.\Data\WI_MIMO_Para_TBS_2600MHz.mat','cir','angle','valid');