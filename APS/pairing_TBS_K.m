%% Spectral Efficiency v.s. Number of Users
Path0 = './Data/GAN/';  % './Data/CNN/' stands for the model used by CNN, './Data/GAN/' stands for the model used by GAN. 
TBS_gen1024 = csvread([Path0,'res_PAS_M1T1_2600MHz.csv'],1,0);
TBS_real1024 = csvread([Path0,'groundtruth_PAS_M1T1_2600MHz.csv'],1,0);
load([Path0,'hvalue_PAS_M1T1_2600MHz.mat']);
H_TBS = h_TBS;
%%
% clear all;
tic;
M = 1024;     % Antenna number
Th = 0.9;    % Threshold
Num_UE = length(H_TBS(:,1)); %Total UE number
SNR_dB = 10;
SNR = 10^(SNR_dB/10);
Num_Antenna = length(H_TBS(1,:));
mean_h2 = norm(H_TBS, 'fro')^2/Num_UE;
K_UE = 10:10:100;   % UE number
K_NUM = length(K_UE);
N_run = 1000;
SINR_thres = 2e-1;

n_colors_gen_average = zeros(K_NUM, 1);
n_colors_real_average = zeros(K_NUM, 1);
rate_gen_average = zeros(K_NUM, 1);
rate_real_average = zeros(K_NUM, 1);
rate_ungroup_average = zeros(K_NUM, 1);
rate_pilot_average = zeros(K_NUM, 1);
rate_group1_gen_average = zeros(K_NUM, 1);
rate_group1_real_average = zeros(K_NUM, 1);

w_dft = dftmtx(M);
w_dft =w_dft(:, 1:Num_Antenna);

for K_index = 1:K_NUM
    K = K_UE(K_index);
    for simulation_run = 1:N_run
        %% Generate user channels
        random_index = randi(Num_UE, K, 1);
        h = H_TBS(random_index, :);
        TBS_APS_gen = TBS_gen1024(random_index, :).^2;
        TBS_APS_real = TBS_real1024(random_index,:).^2;
        vec_gen = zeros(K,M);
        vec_real = zeros(K,M);
        for k = 1:K
            APS = TBS_APS_gen(k, :);
            APS_total = sum(APS);
            step = max(APS)/100;
            val = step;
            while(sum(APS(APS>val))>Th*APS_total)
                val = val+step;
            end
            val = val-step;
            vec_gen(k,:) = APS>val;
            %         figure;
            %         plot(1:1024, APS, 'b');
            %         hold on;
            %         plot(1:1024, val*ones(1,1024), 'b--');
            
            APS = TBS_APS_real(k, :);
            APS_total = sum(APS);
            step = max(APS)/100;
            val = step;
            while(sum(APS(APS>val))>Th*APS_total)
                val = val+step;
            end
            val = val-step;
            vec_real(k,:) = APS>val;
            %         plot(1:1024, APS, 'r');
            %         plot(1:1024, val*ones(1,1024), 'r--');
            
        end
        conflict_gen = (vec_gen*vec_gen')>10;
        conflict_real = (vec_real*vec_real')>10;
        
        %% Coloring and grouping
        %     [coloring,n_colors] = dsatur(conflict);
        [coloring_gen,n_colors_gen] = greedy_naive(conflict_gen);
        [coloring_real,n_colors_real] = greedy_naive(conflict_real);
        n_colors_gen_average(K_index) = n_colors_gen_average(K_index) + n_colors_gen;
        n_colors_real_average(K_index) = n_colors_real_average(K_index) + n_colors_real;
        
        %% Calculate sum rate
        rate_gen = 0;
        rate_real = 0;
        rate_ungroup = 0;
        rate_pilot = 0;
        rate_group1_gen = 0;
        rate_group1_real = 0;
        % rate_gen
        for color = 1:n_colors_gen
            user_index = find(coloring_gen == color);
            user_num = length(user_index);
            APS = TBS_APS_gen(user_index, :);
            [~, w_I] = max(APS, [], 2);
            w_ue = w_dft(w_I, :);
            h_ue = h(user_index, :);
            denominator_K = norm(w_ue, 'fro');
            for m = 1:user_num
                S = abs(w_ue(m, :)*h_ue(m, :).')^2;
                I = sum(abs(w_ue*h_ue(m, :).').^2) - S;
                SINR = S/(I + mean_h2*denominator_K^2/SNR);
                if(SINR>SINR_thres)
                    rate_gen = rate_gen + user_num*log2(1+SINR);
                end
            end
        end
        rate_gen_average(K_index) = rate_gen_average(K_index) + rate_gen/K;
        
        % rate_real
        for color = 1:n_colors_real
            user_index = find(coloring_real == color);
            user_num = length(user_index);
            APS = TBS_APS_real(user_index, :);
            [~, w_I] = max(APS, [], 2);
            w_ue = w_dft(w_I, :);
            h_ue = h(user_index, :);
            denominator_K = norm(w_ue, 'fro');
            for m = 1:user_num
                S = abs(w_ue(m, :)*h_ue(m, :).')^2;
                I = sum(abs(w_ue*h_ue(m, :).').^2) - S;
                SINR = S/(I + mean_h2*denominator_K^2/SNR);
                if(SINR>SINR_thres)
                    rate_real = rate_real + user_num*log2(1+SINR);
                end
            end
        end
        rate_real_average(K_index) = rate_real_average(K_index) + rate_real/K;
        
        %rate_ungroup
        [~, w_I] = max(TBS_APS_real, [], 2);
        w_ue = w_dft(w_I, :);
        for m = 1:K
            S = abs(w_ue(m, :)*h(m, :).')^2;
            denominator_K = norm(w_ue(m, :));
            SINR = S/(mean_h2*denominator_K^2/SNR);
            if(SINR>SINR_thres)
                rate_ungroup = rate_ungroup + log2(1+SINR);
            end
        end
        rate_ungroup_average(K_index) = rate_ungroup_average(K_index) + rate_ungroup/K;
        
        %rate group1 gen
        user_num = K;
        APS = TBS_APS_gen;
        [~, w_I] = max(APS, [], 2);
        w_ue = w_dft(w_I, :);
        h_ue = h;
        denominator_K = norm(w_ue, 'fro');
        for m = 1:user_num
            S = abs(w_ue(m, :)*h_ue(m, :).')^2;
            I = sum(abs(w_ue*h_ue(m, :).').^2) - S;
            SINR = S/(I + mean_h2*denominator_K^2/SNR);
            if(SINR>SINR_thres)
                rate_group1_gen = rate_group1_gen + user_num*log2(1+SINR);
            end
        end
        rate_group1_gen_average(K_index) = rate_group1_gen_average(K_index) + rate_group1_gen/K;
        
        %rate group1 real
        user_num = K;
        APS = TBS_APS_real;
        [~, w_I] = max(APS, [], 2);
        w_ue = w_dft(w_I, :);
        h_ue = h;
        denominator_K = norm(w_ue, 'fro');
        for m = 1:user_num
            S = abs(w_ue(m, :)*h_ue(m, :).')^2;
            I = sum(abs(w_ue*h_ue(m, :).').^2) - S;
            SINR = S/(I + mean_h2*denominator_K^2/SNR);
            if(SINR>SINR_thres)
                rate_group1_real = rate_group1_real + user_num*log2(1+SINR);
            end
        end
        rate_group1_real_average(K_index) = rate_group1_real_average(K_index) + rate_group1_real/K;
        
        
    end
end

toc;

n_colors_gen_average = n_colors_gen_average/N_run
n_colors_real_average = n_colors_real_average/N_run
rate_gen_average = rate_gen_average/N_run;
rate_real_average = rate_real_average/N_run;
rate_ungroup_average = rate_ungroup_average/N_run;
rate_group1_gen_average = rate_group1_gen_average/N_run;
rate_group1_real_average= rate_group1_real_average/N_run;

figure;
plot(K_UE, rate_gen_average, 'b*-');
hold on;
plot(K_UE, rate_real_average, 'ro-');
plot(K_UE, rate_ungroup_average, 'k--');
plot(K_UE, rate_group1_gen_average, 'g-.');
plot(K_UE, rate_group1_real_average, 'm-.');