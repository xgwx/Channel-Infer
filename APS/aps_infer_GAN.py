# -*- coding: utf-8 -*-
import scipy.misc
import numpy as np
import os 
import tensorflow as tf
from tensorflow.contrib.layers.python.layers import batch_norm as batch_norm
from sklearn.model_selection import train_test_split
import scipy.io as sio  
from sklearn.metrics import mean_squared_error

os.environ['CUDA_VISIBLE_DEVICES']='0'

route = './Data/PAS_M1T1_2600MHz.mat'
Data = sio.loadmat(route)
CBS_raw = Data['H_CBS']     #CSI of CBS
TBS_raw = Data['H_TBS']    #CSI of TBS 

mask = [True]*CBS_raw.shape[0]
for i in range(np.shape(CBS_raw)[0]):
    CBS_norm_temp = np.sum(np.abs(CBS_raw[i,:]))
    TBS_norm_temp = np.sum(np.abs(TBS_raw[i,:]))
    if CBS_norm_temp == 0.0 or TBS_norm_temp == 0.0:
        mask[i] = False

CBS = CBS_raw[mask, :]
TBS = TBS_raw[mask, :]

Num_fft = 1024
Num_zero = Num_fft - np.shape(TBS)[1]
TBS = np.concatenate((TBS, np.zeros((np.shape(TBS)[0], Num_zero))*complex(1,0)), axis = 1)

N_TBS = int(np.shape(TBS)[1])
dftmtx = np.fft.fft(np.eye(N_TBS))
for i in range(np.shape(TBS)[0]):
    y_temp = TBS[i,:].reshape([N_TBS,1])
    y_temp = np.dot(dftmtx,y_temp)
    TBS[i,:] = np.abs(y_temp).reshape([N_TBS])

N_CBS = int(np.shape(CBS)[1])
dftmtx = np.fft.fft(np.eye(N_CBS))
for i in range(np.shape(CBS)[0]):
    y_temp = CBS[i,:].reshape([N_CBS,1])
    y_temp = np.dot(dftmtx,y_temp)
    CBS[i,:] = np.abs(y_temp).reshape([N_CBS])

#ȡlog10
CBS = np.abs(CBS)
TBS = np.abs(TBS)
CBS[np.where(CBS == 0.0)] = 1e-30
TBS[np.where(TBS == 0.0)] = 1e-30
CBS = np.log10(CBS)
TBS = np.log10(TBS)

def data_normalize(train, test):
    mean = np.reshape(np.mean(train, axis = 0), (1, np.shape(train)[1]))
    std = np.reshape(np.std(train, axis = 0), (1, np.shape(train)[1]))
    train = (train - mean) / std
    test = (test - mean) / std
    return train, test

X_con = np.concatenate((TBS, TBS_raw[mask, :]), axis=1)
X_train_t, X_test_t, Y_train, Y_test = train_test_split(X_con, CBS, test_size=0.1, random_state=26)
X_train = np.real(X_train_t[:,0:np.shape(TBS)[1]])
X_test = np.real(X_test_t[:,0:np.shape(TBS)[1]])
h_test = X_test_t[:,np.shape(TBS)[1]:]

X_train_index = np.argmax(X_train, axis = 1)
X_test_index = np.argmax(X_test, axis = 1)

X_mean = np.reshape(np.mean(X_train, axis = 0), (1, np.shape(X_train)[1]))
X_std = np.reshape(np.std(X_train, axis = 0), (1, np.shape(X_train)[1]))
Y_mean = np.reshape(np.mean(Y_train, axis = 0), (1, np.shape(Y_train)[1]))
Y_std = np.reshape(np.std(Y_train, axis = 0), (1, np.shape(Y_train)[1]))

Num_train = np.shape(X_train)[0]
Num_test = np.shape(X_test)[0]
Y_SIZE = np.shape(Y_train)[1]
X_SIZE = np.shape(X_train)[1]

X_train, X_test = data_normalize(X_train, X_test)
Y_train, Y_test = data_normalize(Y_train, Y_test)

def linear_layer(value, keep_prob, output_dim, name = 'linear_connected'):
    with tf.variable_scope(name):
        try:
            weights = tf.get_variable('weights', 
                [int(value.get_shape()[1]), output_dim], 
                initializer = tf.truncated_normal_initializer(stddev = 0.02))
            biases = tf.get_variable('biases', 
                [output_dim], initializer = tf.constant_initializer(0.0))
        except ValueError:
            tf.get_variable_scope().reuse_variables()
            weights = tf.get_variable('weights', 
                [int(value.get_shape()[1]), output_dim], 
                initializer = tf.truncated_normal_initializer(stddev = 0.02))
            biases = tf.get_variable('biases', 
                [output_dim], initializer = tf.constant_initializer(0.0))
        x = tf.nn.dropout(value, keep_prob)
        return tf.matmul(x, weights) + biases

def lrelu(x, leak = 0.2, name = 'lrelu'):
    with tf.variable_scope(name):
        return tf.maximum(x, x*leak, name = name)
    
def batch_norm_layer(value, is_train = True, name = 'batch_norm', reuse = False):
    with tf.variable_scope(name) as scope:
        if is_train:
            return batch_norm(value, decay = 0.9, epsilon = 1e-5, scale = True,
                                is_training = is_train, reuse = reuse, updates_collections = None, scope = scope)
        else :
            return batch_norm(value, decay = 0.9, epsilon = 1e-5, scale = True,
                            is_training = is_train, reuse = True,
                            updates_collections = None, scope = scope)

def conv2d(value, keep_prob, output_dim, k_h = 5, k_w = 1, strides = [1,1,1,1], name = "conv2d"):
    with tf.variable_scope(name):
        try:
            weights = tf.get_variable('weights', 
                [k_h, k_w, int(value.get_shape()[-1]), output_dim],
                initializer = tf.truncated_normal_initializer(stddev = 0.02))
            biases = tf.get_variable('biases',
                [output_dim], initializer = tf.constant_initializer(0.0))
        except ValueError:
            tf.get_variable_scope().reuse_variables()
            weights = tf.get_variable('weights', 
                [k_h, k_w, int(value.get_shape()[-1]), output_dim],
                initializer = tf.truncated_normal_initializer(stddev = 0.02))
            biases = tf.get_variable('biases',
                [output_dim], initializer = tf.constant_initializer(0.0))
        conv = tf.nn.conv2d(value, weights, strides = strides, padding = "SAME")
        conv = tf.reshape(tf.nn.bias_add(conv, biases), tf.shape(conv))
        x = tf.nn.dropout(conv, keep_prob)
        return x

def deconv2d(value, keep_prob, output_shape, k_h = 5, k_w = 1, strides = [1,1,1,1], name = "deconv2d"):
    with tf.variable_scope(name):
        try:
            weights = tf.get_variable('weights',
                [k_h, k_w, output_shape[-1], int(value.get_shape()[-1])],
                initializer = tf.truncated_normal_initializer(stddev = 0.02))
            biases = tf.get_variable('biases',
                [output_shape[-1]], initializer = tf.constant_initializer(0.0))
        except ValueError:
            tf.get_variable_scope().reuse_variables()
            weights = tf.get_variable('weights',
                [k_h, k_w, output_shape[-1], int(value.get_shape()[-1])],
                initializer = tf.truncated_normal_initializer(stddev = 0.02))
            biases = tf.get_variable('biases',
                [output_shape[-1]], initializer = tf.constant_initializer(0.0))
        deconv = tf.nn.conv2d_transpose(value, weights, output_shape, strides = strides)
        deconv = tf.reshape(tf.nn.bias_add(deconv, biases), tf.shape(deconv))
        x = tf.nn.dropout(deconv, keep_prob)
        return x

def conv_cond_concat(value, cond, name = 'concat'):
    value_shapes = tf.reshape(tf.shape(value),[-1])
    cond_shapes = tf.reshape(tf.shape(cond), [-1])
    with tf.variable_scope(name):
        return tf.concat([value, cond * tf.ones([value_shapes[0], cond_shapes[1], value_shapes[2], value_shapes[3]])], 1, name = name)
        #return tf.concat([value, cond * tf.ones([value_shapes[0], value_shapes[1], value_shapes[2], cond_shapes[3]])], 3, name = name)


BATCH_SIZE = None

def generator(y, keep_prob, reuse = False, train = True):
    if reuse:
        tf.get_variable_scope().reuse_variables()
        
    #yb = tf.reshape(y, [BATCH_SIZE, np.shape(y)[1]], name = 'g_yb')
    # z_y = tf.concat([z,y], 1, name = 'g_z_concat_y')
    # linear1 = linear_layer(z_y, 1, 100, name = 'g_linear_layer1')
    linear1 = linear_layer(y, 1, 100, name = 'g_linear_layer1')
    bn1 = tf.nn.relu(batch_norm_layer(linear1, is_train = train, name = 'g_bn1'))

    bn1_y = tf.concat([bn1, y], 1 ,name = 'g_bn1_concat_y')
    linear2 = linear_layer(bn1_y, keep_prob, 128, name = 'g_linear_layer2')
    bn2 = tf.nn.relu(batch_norm_layer(linear2, is_train = train, name = 'g_bn2'))
    
    bn2_y = tf.concat([bn2, y], 1 ,name = 'g_bn2_concat_y')
    linear3 = linear_layer(bn2_y, keep_prob, 256, name = 'g_linear_layer3')
    bn3 = tf.nn.relu(batch_norm_layer(linear3, is_train = train, name = 'g_bn3'))
    
    bn3_y = tf.concat([bn3, y], 1 ,name = 'g_bn3_concat_y')
    linear4 = linear_layer(bn3_y, keep_prob, 512, name = 'g_linear_layer4')
    bn4 = tf.nn.relu(batch_norm_layer(linear4, is_train = train, name = 'g_bn4'))
    
    bn4_y = tf.concat([bn4, y], 1 ,name = 'g_bn4_concat_y')
    linear5 = linear_layer(bn4_y, keep_prob, 1024, name = 'g_linear_layer5')
    bn5 = tf.nn.relu(batch_norm_layer(linear5, is_train = train, name = 'g_bn5'))
    
    bn5_y = tf.concat([bn5, y], 1 ,name = 'g_bn5_concat_y')
    linear6 = linear_layer(bn5_y, 1, X_SIZE, name = 'g_linear_layer6')
    
    return linear6

def generator_cnn(y, keep_prob, reuse = False, train = True):
    if reuse:
        tf.get_variable_scope().reuse_variables()
        
    yb = tf.reshape(y, [tf.shape(y)[0], Y_SIZE, 1, 1], name = 'g_yb')
    #yb = tf.reshape(y, [tf.shape(y)[0], 1, 1, Y_SIZE], name = 'g_yb')
    conv1 = conv2d(yb, 1.0, 16, strides = [1, 1, 1, 1], name = 'g_conv1')
    bn1 = batch_norm_layer(conv1, is_train = train, name = 'g_bn1')
    lr1 = lrelu(bn1, name = 'g_lrelu1')
    print(tf.shape(lr1))
    conv2 = conv2d(lr1, 1.0, 32, strides = [1, 1, 1, 1], name = 'g_conv2')
    bn2 = batch_norm_layer(conv2, is_train = train, name = 'g_bn2')
    lr2 = lrelu(bn2, name = 'g_lrelu2')
    print(tf.shape(lr2))
    conv3 = conv2d(lr2, 1.0, 64, strides = [1, 1, 1, 1], name = 'g_conv3')
    bn3 = batch_norm_layer(conv3, is_train = train, name = 'g_bn3')
    lr3 = lrelu(bn3, name = 'g_lrelu3')
    print(tf.shape(lr3))
    conv4 = conv2d(lr3, 1.0, 128, strides = [1, 1, 1, 1], name = 'g_conv4')
    bn4 = batch_norm_layer(conv4, is_train = train, name = 'g_bn4')
    lr4 = lrelu(bn4, name = 'g_lrelu4')
    print(tf.shape(lr4))
    conv5 = conv2d(lr4, 1.0, 32, strides = [1, 1, 1, 1], name = 'g_conv5')
    bn5 = batch_norm_layer(conv5, is_train = train, name = 'g_bn5')
    lr5 = lrelu(bn5, name = 'g_lrelu5')
    print(tf.shape(lr5))
    conv6 = conv2d(lr5, 1.0, 16, strides = [1, 1, 1, 1], name = 'g_conv6')
    bn6 = batch_norm_layer(conv6, is_train = train, name = 'g_bn6')
    lr6 = lrelu(bn6, name = 'g_lrelu6')
    print(tf.shape(lr6))
    lr6_re = tf.reshape(lr6, [tf.shape(y)[0], lr6.get_shape().as_list()[1]
                              *lr6.get_shape().as_list()[2]*lr6.get_shape().as_list()[3]], name = 'g_lr6_reshape')
    lr6_y = tf.concat([lr6_re, y], 1, name = 'g_lr6_concat_y')
    linear1 = linear_layer(lr6_y, keep_prob, 1024, name = 'g_linear_layer1')
    bn7 = batch_norm_layer(linear1, is_train = train, name = 'g_bn7')
    lr7 = lrelu(bn7, name = 'g_lrelu7')
    print(tf.shape(lr7))
    linear2 = linear_layer(lr7, 1, X_SIZE, name = 'g_linear_layer3')
    
    return linear2

def discriminator(X, y, keep_prob, reuse = False, train = True):
    if reuse:
        tf.get_variable_scope().reuse_variables()

    #yb = tf.reshape(y, [BATCH_SIZE, 1, 1, 10], name = 'd_yb')
    X_y = tf.concat([X, y], 1, name = 'd_concat_y')
    linear1 = linear_layer(X_y, 1, 512, name = 'd_linear_layer1')
    bn1 = batch_norm_layer(linear1, is_train = train, name = 'd_bn1', reuse = reuse)
    lr1 = lrelu(bn1, name = 'd_lrelu1')

    lr1_y = tf.concat([lr1, y], 1, name = 'd_lr1_concat_y')
    linear2 = linear_layer(lr1_y, keep_prob, 256, name = 'd_linear_layer2')
    bn2 = batch_norm_layer(linear2, is_train = train, name = 'd_bn2', reuse = reuse)
    lr2 = lrelu(bn2, name = 'd_lrelu2')
    
    lr2_y = tf.concat([lr2, y], 1, name = 'd_lr2_concat_y')
    linear3 = linear_layer(lr2_y, keep_prob, 128, name = 'd_linear_layer3')
    bn3 = batch_norm_layer(linear3, is_train = train, name = 'd_bn3', reuse = reuse)
    lr3 = lrelu(bn3, name = 'd_lrelu3')
    
    lr3_y = tf.concat([lr3, y], 1, name = 'd_lr3_concat_y')
    linear4 = linear_layer(lr3_y, keep_prob, 64, name = 'd_linear_layer4')
    bn4 = batch_norm_layer(linear4, is_train = train, name = 'd_bn4', reuse = reuse)
    lr4 = lrelu(bn4, name = 'd_lrelu4')
    
    lr4_y = tf.concat([lr4, y], 1, name = 'd_lr4_concat_y')
    linear5 = linear_layer(lr4_y, 1, 1, name = 'd_linear_layer5')
    bn5 = batch_norm_layer(linear5, is_train = train, name = 'd_bn5', reuse = reuse)
    
    return tf.nn.sigmoid(bn5)

def discriminator_cnn(X, y, keep_prob, reuse = False, train = True):
    if reuse:
        tf.get_variable_scope().reuse_variables()

    #yb = tf.reshape(y, [BATCH_SIZE, 1, 1, 10], name = 'd_yb')
    X_y = tf.concat([X, y], 1, name = 'd_concat_y')
    conv1 = conv2d(X_y, 1.0, 128, strides = [1, 1, 1, 1], name = 'd_conv1')
    bn1 = batch_norm_layer(conv1, is_train = train, name = 'd_bn1')
    lr1 = lrelu(bn1, name = 'd_lrelu1')
    
    lr1_y = tf.concat([lr1, y], 1, name = 'd_lr1_concat_y')
    conv2 = conv2d(lr1_y, 1.0, 64, strides = [1, 1, 1, 1], name = 'd_conv2')
    bn2 = batch_norm_layer(conv2, is_train = train, name = 'd_bn2')
    lr2 = lrelu(bn2, name = 'd_lrelu2')
    
    lr2_y = tf.concat([lr2, y], 1, name = 'd_lr2_concat_y')
    conv3 = conv2d(lr2_y, 1.0, 32, strides = [1, 1, 1, 1], name = 'd_conv3')
    bn3 = batch_norm_layer(conv3, is_train = train, name = 'd_bn3')
    lr3 = lrelu(bn3, name = 'd_lrelu3')
    
    lr3_y = tf.concat([lr3, y], 1, name = 'd_lr3_concat_y')
    linear4 = linear_layer(lr3_y, 1, 1, name = 'd_linear_layer4')
    bn4 = batch_norm_layer(linear4, is_train = train, name = 'd_bn4', reuse = reuse)
    
    return tf.nn.sigmoid(bn4)

gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.5)

tf.reset_default_graph()
        
#global_step to record the step of training
global_step = tf.Variable(0, name = 'global_step', trainable = True)


#set the data placeholder
y = tf.placeholder(tf.float32, [BATCH_SIZE, Y_SIZE], name = 'y')
X = tf.placeholder(tf.float32, [BATCH_SIZE, X_SIZE], name = 'X')
keep_prob = tf.placeholder(tf.float32, name = 'keep_prob')


#model
G = generator_cnn(y, keep_prob)
#test model
with tf.variable_scope(tf.get_variable_scope(), reuse = True):
    _G = generator_cnn(y, keep_prob, reuse = True, train = False)
#train real data
D = discriminator(X, y, keep_prob)
#train generated data
with tf.variable_scope(tf.get_variable_scope(), reuse = True):
    _D = discriminator(G, y, keep_prob, reuse = True)

#calculate loss using sigmoid cross entropy
d_loss_real = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits = D, labels = tf.ones_like(D)))
d_loss_fake = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits = _D, labels = tf.zeros_like(_D)))
g_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits = _D, labels = tf.ones_like(_D)))
d_loss = d_loss_real + d_loss_fake

mse = tf.reduce_mean(tf.square(G - X))

t_vars = tf.trainable_variables()
d_vars = [var for var in t_vars if 'd_' in var.name]
g_vars = [var for var in t_vars if 'g_' in var.name]

with tf.variable_scope(tf.get_variable_scope(), reuse = False):
    d_optim = tf.train.AdamOptimizer(0.0001, beta1 = 0.5).minimize(d_loss, var_list = d_vars, global_step = global_step)
    g_optim = tf.train.AdamOptimizer(0.0001, beta2 = 0.5).minimize(g_loss+0.2*mse, var_list = g_vars, global_step = global_step)

#initial 
import time
init = tf.global_variables_initializer()
sess = tf.InteractiveSession()
model_dir = './Model/PAS_M1T1_2600MHz_GAN_cnng6d4_1024/'
model_reload = False
saver = tf.train.Saver()
ckpt = tf.train.get_checkpoint_state(model_dir)
if ckpt and model_reload and tf.train.checkpoint_exists(ckpt.model_checkpoint_path):
    print('Reloading model parameters..')
    saver.restore(sess, save_path=ckpt.model_checkpoint_path)   
else:
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    print('Created new model parameters..')
    sess.run(init)
    
#save
saver = tf.train.Saver()
#check_path = "GAN_save/model.ckpt"


#run
sess.run(init)
#saver.restore(sess.check_path)

test_mse = np.zeros([500])
test_dbmse = np.zeros([500])
test_time = np.zeros([500])
#saver.restore(sess.check_path)

batch_size = 5000

start = time.time()

#train
for epoch in range(500):
    batch_idx = int(Num_train/batch_size)
    for idx in range(batch_idx):
        batch_TBS = X_train[idx*batch_size:(idx+1)*batch_size, :]
        batch_CBS = Y_train[idx*batch_size:(idx+1)*batch_size, :]
        batch_TBS_index = X_train_index[idx*batch_size:(idx+1)*batch_size]
        sess.run(d_optim,
                 feed_dict = {X: batch_TBS,
                              y: batch_CBS,
                              keep_prob: 0.7})

        sess.run(g_optim,
                 feed_dict = {X: batch_TBS,
                              y: batch_CBS,
                              keep_prob: 0.7})

        batch_TBS_generator = sess.run(G, feed_dict={y: batch_CBS, keep_prob: 1.0})

        d_loss1 = d_loss_fake.eval({y: batch_CBS, keep_prob: 1.0})
        d_loss2 = d_loss_real.eval({X: batch_TBS, y:batch_CBS, keep_prob: 1.0})
        D_loss = d_loss1 + d_loss2
        G_loss = g_loss.eval({y: batch_CBS, keep_prob: 1.0})

        batch_TBS = batch_TBS*X_std + X_mean
        batch_TBS_generator = batch_TBS_generator*X_std + X_mean
        batch_TBS_gen_index = np.argmax(batch_TBS_generator, axis = 1)

        accuracy = np.sum(np.equal(batch_TBS_gen_index, batch_TBS_index))/batch_size

        idx_diff = np.abs(batch_TBS_gen_index - batch_TBS_index)
        idx_diff[np.where(idx_diff > Num_fft/2)] = Num_fft - idx_diff[np.where(idx_diff > Num_fft/2)]
        idx_diff = np.mean(idx_diff)

        MSE = mean_squared_error(batch_TBS, batch_TBS_generator)  
        Relative_Error = np.mean(np.true_divide(np.fabs(batch_TBS - batch_TBS_generator), np.fabs(batch_TBS)))

        batch_TBS_real = np.power(10, np.square(batch_TBS))
        batch_TBS_generator_real = np.power(10, np.square(batch_TBS_generator))
        
        '''norm1 = np.sum(np.square(batch_TBS_real-batch_TBS_generator_real), axis=1)
        norm2 = np.sum(np.square(batch_TBS_real), axis=1)
        Error = np.mean(np.true_divide(norm1, norm2))
        Error_dB = 10*np.log10(Error)'''
        #every 20 batch output loss
        '''if idx % 20 == 0:
            print("Epoch: %d [%4d/%4d] accuracy: %.8f, idx_error: %.8f, MSE: %.8f, RE: %.8f"\
                  % (epoch, idx, batch_idx, accuracy, idx_diff, MSE, Relative_Error))'''
    TBS_recover = sess.run(_G, feed_dict={y: Y_test, keep_prob: 1.0})
    TBS_recover = TBS_recover*X_std + X_mean
    TBS_recover_exp = np.power(10, 2*TBS_recover)
    TBS_true = X_test*X_std + X_mean
    TBS_true_exp = np.power(10, 2*TBS_true)
    test_dbmse[epoch] = np.mean(np.square(TBS_true-TBS_recover))
    test_mse[epoch] = np.mean(np.square(TBS_true_exp-TBS_recover_exp))
    test_time[epoch] = time.time() - start
    print('Epoch done test_mse: %.6f'%(test_dbmse[epoch]))
    if(epoch % 20 == 0):
         saver.save(sess, save_path=model_dir, global_step=epoch)
saver.save(sess, save_path=model_dir, global_step=epoch)
#test
print("Test begins:")
accuracy_test = 0
idx_diff_test = 0
batch_size = 5000
batch_idx = int(Num_test/batch_size)
for idx in range(batch_idx):
    batch_TBS = X_test[idx*batch_size:(idx+1)*batch_size, :]
    batch_CBS = Y_test[idx*batch_size:(idx+1)*batch_size, :]
    batch_TBS_index = X_test_index[idx*batch_size:(idx+1)*batch_size]

    batch_TBS_generator = sess.run(_G, feed_dict={y: batch_CBS, keep_prob: 1.0})

    batch_TBS = batch_TBS*X_std + X_mean
    batch_TBS_generator = batch_TBS_generator*X_std + X_mean
    batch_TBS_gen_index = np.argmax(batch_TBS_generator, axis = 1)

    accuracy = np.sum(np.equal(batch_TBS_gen_index, batch_TBS_index))/batch_size
    accuracy_test = accuracy_test + np.sum(np.equal(batch_TBS_gen_index, batch_TBS_index))

    idx_diff = np.abs(batch_TBS_gen_index - batch_TBS_index)
    idx_diff[np.where(idx_diff > Num_fft/2)] = Num_fft - idx_diff[np.where(idx_diff > Num_fft/2)]
    idx_diff_test = idx_diff_test + np.sum(idx_diff)
    idx_diff = np.mean(idx_diff)

    MSE = mean_squared_error(batch_TBS, batch_TBS_generator)  
    Relative_Error = np.mean(np.true_divide(np.fabs(batch_TBS - batch_TBS_generator), np.fabs(batch_TBS)))

    batch_TBS_real = np.power(10, np.square(batch_TBS))
    batch_TBS_generator_real = np.power(10, np.square(batch_TBS_generator))

    norm1 = np.sum(np.square(batch_TBS_real-batch_TBS_generator_real), axis=1)
    norm2 = np.sum(np.square(batch_TBS_real), axis=1)
    Error = np.mean(np.true_divide(norm1, norm2))
    Error_dB = 10*np.log10(Error)

    print("[%4d/%4d] accuracy: %.8f, idx_error: %.8f, MSE: %.8f, RE: %.8f, Error: %.8f, Error_dB: %.8f"\
            % (idx, batch_idx, accuracy, idx_diff, MSE, Relative_Error, Error, Error_dB))

#sess.close()

sess.close()
sio.savemat('./Data/GAN/mse_gan_result.mat',{'test_mse':test_mse,'test_dbmse':test_dbmse,'test_time':test_time})

import pandas as pandas
sess = tf.InteractiveSession()
##save
model_dir = './Model/PAS_M1T1_2600MHz_GAN_cnng6d4_1024/'
model_reload = True
saver = tf.train.Saver()
ckpt = tf.train.get_checkpoint_state(model_dir)
if ckpt and model_reload and tf.train.checkpoint_exists(ckpt.model_checkpoint_path):
    print('Reloading model parameters..')
    saver.restore(sess, save_path=ckpt.model_checkpoint_path)   
else:
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    print('Created new model parameters..')
    sess.run(init)
TBS_recover = sess.run(_G, feed_dict={y: Y_test, keep_prob: 1.0})
TBS_recover = TBS_recover*X_std + X_mean
TBS_recover = np.power(10, 2*TBS_recover)
TBS_true = X_test*X_std + X_mean
TBS_true = np.power(10, 2*TBS_true)
dataframe = pd.DataFrame(TBS_recover)
dataframe.to_csv('./Data/GAN/res_PAS_M1T1_2600MHz_GAN_cnng6d4_1024.csv',index=False,sep=',')
dataframe1 = pd.DataFrame(TBS_true)
dataframe1.to_csv('./Data/GAN/groundtruth_PAS_M1T1_2600MHz_GAN_cnng6d4_1024.csv',index=False,sep=',')
dataframe2 = pd.DataFrame(h_test)
dataframe2.to_csv('./Data/GAN/hvalue_PAS_M1T1_2600MHz_GAN_cnng6d4_1024.csv',index=False,sep=',')
sio.savemat('./Data/GAN/hvalue_PAS_M1T1_2600MHz_GAN_cnng6d4_1024.mat', {'H_TBS':h_test})
