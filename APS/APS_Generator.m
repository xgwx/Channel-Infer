%% Setting Channel Parameters
N_CBSA = 128;
N_TBSA = 32;
N_UEA = 1;
N_beampattern = 256;
% Carrier_Freq = 28000e6;
load('.\Data\WI_MIMO_Para_MBS_2600MHz.mat');
angle_c = angle;
cir_c = cir;
valid_c = valid;
load('.\Data\WI_MIMO_Para_TBS_2600MHz.mat');
angle_t = angle;
cir_t = cir;
valid_t = valid;
%load('HO_loc_data');
valid = logical(valid_c.*valid_t);
N_MS = length(valid);
%% PAS Generating
H_CBS = zeros(N_MS,N_CBSA,N_UEA);
H_TBS = zeros(N_MS,N_TBSA,N_UEA);
for i = 1:N_MS
    %generate raw csi data of CBS and TBS
    if(valid(i))
        cbs_tmp = CSI_gscm(cir_c{i},angle_c{i},N_CBSA,N_UEA,0.5);
        H_CBS(i,:,:) = reshape(cbs_tmp,[N_CBSA,N_UEA]);
        tbs_tmp = CSI_gscm(cir_t{i},angle_t{i},N_TBSA,N_UEA,0.5);
        H_TBS(i,:,:) = reshape(tbs_tmp,[N_TBSA,N_UEA]);
    end
end
H_TBS = H_TBS(valid,:);
H_CBS = H_CBS(valid,:);
save('.\Data\PAS_M1T1_U1_2600MHz.mat','H_CBS','H_TBS');

